import csv
def print_counts():
    with open('school_data.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, quotechar='|')
        i,school_count=0,0
        statewise_school_count,metrocentric_locale_count,citywise_school_count = {},{},{}
        for row in spamreader:
            if i>=1:
                if str(row[3]).find('SCHOOL') != -1:
                    school_count += 1
                    if not statewise_school_count.get(str(row[5]),0):
                        statewise_school_count[str(row[5])] = 1
                    else:
                        statewise_school_count[str(row[5])] += 1
                    if not metrocentric_locale_count.get(str(row[8]),0):
                        metrocentric_locale_count[str(row[8])] = 1
                    else:
                        metrocentric_locale_count[str(row[8])] += 1
                    if not citywise_school_count.get(str(row[4]),0):
                        citywise_school_count[str(row[4])] = 1
                    else:
                        citywise_school_count[str(row[4])] += 1
            i += 1
        max_school_count = max(list(citywise_school_count.values()))
        max_school_city = list(citywise_school_count.keys())[list(citywise_school_count.values()).index(max_school_count)]
        atleast_oneschool_city = len(citywise_school_count)
        print("Total schools in dataset - ",school_count)
        print("Statewise school count - ",statewise_school_count)
        print("Metro-centric locale wise - ",metrocentric_locale_count)
        print("City which has most schools - ",max_school_city)
        print("Number of schools in city which has most schools - ",max_school_count)
        print("Number of cities which has atleast one school in it - ",atleast_oneschool_city)
