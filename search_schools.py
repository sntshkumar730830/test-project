import csv
def get_schools(school_name):
    school_name = school_name.replace("SCHOOL","").replace(" ","").lower()
    three_school_list = {}
    with open('school_data.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, quotechar='|')
        i = 0
        for row in spamreader:
            if i>=1:
                if str(row[3].replace("SCHOOL","").replace(" ","").lower()).find(school_name) != -1:
                    if len(three_school_list) <=2:
                        three_school_list[str(row)] = 100
                    else:
                        min_school = min(list(three_school_list.values()))
                        min_school_index = list(three_school_list.values()).index(min_school)
                        min_school_val = list(three_school_list.keys())[min_school_index]
                        del three_school_list[min_school_val]
                        three_school_list[str(row)] = 100
                else:
                    common_string = len(set(list(school_name)) & set(list(str(row[3].replace("SCHOOL","").replace(" ","").lower()))))
                    if common_string > 0:
                        if len(three_school_list) <= 2:
                            three_school_list[str(row)] = common_string
                        else:
                            min_school = min(list(three_school_list.values()))
                            min_school_index = list(three_school_list.values()).index(min_school)
                            min_school_val = list(three_school_list.keys())[min_school_index]
                            del three_school_list[min_school_val]
                            three_school_list[str(row)] = common_string
            i += 1
            if list(three_school_list.values()).count(100) == 3:
                break
        print("Following are your matched results -")
        for each in three_school_list:
            print(each)
